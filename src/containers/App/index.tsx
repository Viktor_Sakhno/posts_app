import React, {useEffect} from 'react';
import {Router, Route, Switch, Redirect} from 'react-router-dom';
import {Posts} from 'containers/Public/Posts';
import {SinglePost} from 'containers/Public/SinglePost';
import {Dimmer, Loader, Message} from 'semantic-ui-react'
import {useStore} from 'store';
import {observer} from 'mobx-react';
import cn from 'classnames';

import history from 'utils/history';

import styles from './styles.module.scss'

export const App = observer(() => {
	const {posts, notification, loader} = useStore();

	useEffect(() => {
		posts.getPosts();
	}, [])

	const closeNotification = () => notification.setCloseNotification();

	return (
		<>
			<Dimmer active={loader.loaderPosts} inverted>
				<Loader size='medium'>Loading</Loader>
			</Dimmer>
			<div className={styles.container}>
				<Message negative
								 onDismiss={closeNotification}
								 className={cn({
									 [styles.notificationMessage]: true,
									 [styles.notificationMessageOpen]: notification.notification,
								 })}
				>
					<Message.Header>{notification.notificationMessage}</Message.Header>
				</Message>
				<Router history={history}>
					<Switch>
						<Route exact path="/posts" component={Posts}/>
						<Route exact path="/posts/:id" component={SinglePost}/>
						<Redirect exact from="/" to="/posts"/>
					</Switch>
				</Router>
			</div>
		</>
	);
})
