import React, {FC, useEffect, useState} from 'react';
import {useParams, useHistory} from 'react-router-dom'
import {observer} from 'mobx-react';
import {useStore} from 'store';
import cn from 'classnames';
import {Comment, Container, Header, Dimmer, Loader, Image, Segment, Button} from 'semantic-ui-react'

import {IComment} from 'models/singlePost';
import {setFirstLetterBig} from 'utils/setFirstLetterBig';

import userPhoto from 'assets/images/userPhoto.jpg'
import userPhotoMain from 'assets/images/userPhotoMain.jpeg'
import shortParagraph from 'assets/images/short-paragraph.png'

import styles from './styles.module.scss'


export const SinglePost: FC = observer(() => {
	const {id} = useParams<{ id: string }>();
	const history = useHistory();
	const {posts, loader} = useStore();
	const [comments, setComments] = useState<IComment[]>([]);

	useEffect(() => {
		getComments(id);
	}, [id]);

	const post = posts.singlePost ? posts.singlePost : JSON.parse(sessionStorage.getItem('singlePost') as string);

	const getComments = async (id: string) => {
		const response = await posts.getPostComments(id);
		response?.data && setComments(response?.data)
	}

	const goBack = () => history.goBack();

	return (
		<Container className={styles.container}>
			<Button className={styles.btn}
							onClick={goBack}
							primary>
				Back to posts
			</Button>
			<Comment.Group className={styles.commentGroup}>
				<Comment>
					<Comment.Avatar src={userPhotoMain}/>
					<Comment.Content>
						<Comment.Author>{setFirstLetterBig(post?.title)}</Comment.Author>
						<Comment.Text>{setFirstLetterBig(post?.body)}</Comment.Text>
					</Comment.Content>
				</Comment>
				<Header as='h3' dividing>
					Comments
				</Header>
				<Segment className={cn({
					[styles.segmentNone]: !loader.loaderComments
				})}>
					<Dimmer active={loader.loaderComments} inverted>
						<Loader inverted content='Loading'/>
					</Dimmer>
					<Image src={shortParagraph}/>
				</Segment>
				{comments.length ?
					comments.map((item) => {
						return (
							<Comment key={item.id}>
								<Comment.Avatar src={userPhoto}/>
								<Comment.Content>
									<Comment.Author as='a'>{item.email}</Comment.Author>
									<Comment.Text>{setFirstLetterBig(item.name)}</Comment.Text>
									<Comment.Text className={styles.postBody}>{setFirstLetterBig(item.body)}</Comment.Text>
								</Comment.Content>
							</Comment>
						);
					}) : null
				}
			</Comment.Group>
		</Container>
	);
})
