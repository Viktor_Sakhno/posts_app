import React, {FC} from 'react';
import {useStore} from 'store';
import {observer} from 'mobx-react';
import {useHistory} from 'react-router-dom';
import {Card, Container} from 'semantic-ui-react';

import {IPost} from 'models/posts';
import {setFirstLetterBig} from 'utils/setFirstLetterBig';

import styles from './styles.module.scss'

export const Posts: FC = observer(() => {
	const {posts} = useStore();
	const history = useHistory();

	const handlerCardClick = (post: IPost) => {
		posts.setSinglePost(post);
		history.push(`/posts/${post.id}`);
	}

	return (
		<Container className={styles.container}>
			<Card.Group className={styles.cardGroup}>
				{posts.posts.length ? posts.posts.map((item) => {
					return (
						<Card key={item.id}
									onClick={() => handlerCardClick(item)}
									className={styles.card}
						>
							<Card.Content className={styles.headerContent}>
								<Card.Header className={styles.cardHeader}>{setFirstLetterBig(item.title)}</Card.Header>
							</Card.Content>
							<Card.Content className={styles.cardContent}>
								{setFirstLetterBig(item.body)}
							</Card.Content>
						</Card>
					);
				}) : null}
			</Card.Group>
		</Container>
	);
})
