import {createContext, useContext} from 'react'
import {configure, observable} from 'mobx'
import remotedev from 'mobx-remotedev'

import posts from './posts'
import loader from './loader'
import notification from './notification'

configure({enforceActions: 'observed'})

@remotedev({global: true})
class RootStore {
	@observable posts = posts
	@observable loader = loader
	@observable notification = notification
}

const store = new RootStore()

export const StoreContext = createContext<RootStore>(store)

export const useStore = (): RootStore => {
	const store = useContext(StoreContext)
	if (!store) {
		throw new Error('useStore must be used within a StoreProvider')
	}
	return store
}

export default store
