import {observable, action, makeAutoObservable} from 'mobx';


class Notification {
	@observable notification: boolean = false;
	@observable notificationMessage: string = 'Error fetch data';

	constructor() {
		makeAutoObservable(this)
	}

	@action
	setOpenNotification() {
		this.notification = true;

		setTimeout(() => {
			this.notification = false;
		}, 3000)
	}

	@action
	setCloseNotification() {
		this.notification = false;
	}

	@action
	setNotificationMessage(value: string) {
		this.notificationMessage = value;
	}
}

export default new Notification()