import {observable, action, makeAutoObservable} from 'mobx';
import {runInAction} from "mobx"
import {api} from '../utils/config';
import {IPost} from '../models/posts';
import Loader from './loader'
import Notification from './notification'

class Posts {
	@observable posts: IPost[] = [];
	@observable singlePost: IPost | null = null;

	constructor() {
		makeAutoObservable(this)
	}

	@action
	async getPosts() {
		try {
			Loader.setLoaderPosts(true);
			const response = await api.get(`https://jsonplaceholder.typicode.com/posts`);
			runInAction(() => {
				this.posts = response.data;
			});
		} catch (e) {
			Notification.setOpenNotification();
			console.log(e);
		} finally {
			Loader.setLoaderPosts(false);
		}
	}

	@action
	async getPostComments(id: string) {
		try {
			Loader.setLoaderComments(true);
			return await api.get(`https://jsonplaceholder.typicode.com/posts/${id}/comments`);
		} catch (e) {
			Notification.setOpenNotification();
			console.log(e)
		} finally {
			Loader.setLoaderComments(false);
		}
	}

	@action
	setSinglePost(post: IPost) {
		sessionStorage.setItem('singlePost', JSON.stringify(post))
		this.singlePost = post;
	}
}

export default new Posts()