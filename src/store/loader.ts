import {observable, action, makeAutoObservable} from 'mobx';


class Loader {
	@observable loaderPosts: boolean = false;
	@observable loaderComments: boolean = false;

	constructor() {
		makeAutoObservable(this)
	}

	@action
	setLoaderPosts(value: boolean) {
		this.loaderPosts = value;
	}

	@action
	setLoaderComments(value: boolean) {
		this.loaderComments = value;
	}
}

export default new Loader()