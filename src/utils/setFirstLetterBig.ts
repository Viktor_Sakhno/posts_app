export const setFirstLetterBig = (str: string) => {
	return str.charAt(0).toUpperCase() + str.slice(1);
}